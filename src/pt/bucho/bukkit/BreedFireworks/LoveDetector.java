package pt.bucho.bukkit.BreedFireworks;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.meta.FireworkMeta;

public class LoveDetector implements Listener{

	private BreedFireworks plugin;
	
	public LoveDetector(BreedFireworks plugin){
		this.plugin = plugin;
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onCreatureSpawn(CreatureSpawnEvent e){
		
		if(!plugin.activated) return;
		
		LivingEntity le = e.getEntity();
		SpawnReason sr = e.getSpawnReason();
		if(!sr.equals(SpawnReason.BREEDING)) return;

		Firework fw = (Firework) le.getWorld().spawnEntity(le.getLocation(), EntityType.FIREWORK);
		FireworkMeta fwmeta = fw.getFireworkMeta();
		FireworkEffect.Builder builder = FireworkEffect.builder();
		builder.withTrail();
		builder.withFlicker();
		builder.withColor(Color.FUCHSIA);
		//		 builder.withColor(Color.GREEN);
		builder.withColor(Color.LIME);
		//		 builder.withColor(Color.BLUE);
		builder.withColor(Color.MAROON);
		//		 builder.withColor(Color.ORANGE);
		builder.withColor(Color.PURPLE);
		//		 builder.withColor(Color.WHITE);
		builder.with(FireworkEffect.Type.BURST);
		fwmeta.addEffects(new FireworkEffect[] { builder.build() });
		fwmeta.setPower((int).9);
		fw.setFireworkMeta(fwmeta);
	}

}
