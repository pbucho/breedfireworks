package pt.bucho.bukkit.BreedFireworks;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

public class BreedFireworks extends JavaPlugin{
	
	private LoveDetector ld = new LoveDetector(this);
	protected boolean activated = true;
	protected File config = new File(getDataFolder() + File.separator + "config.yml");
	protected Logger log = Bukkit.getLogger();
	
	@Override
	public void onEnable(){
		Bukkit.getPluginManager().registerEvents(ld, this);
		if(!config.exists()){
			log.info("No config file detected. Creating a new one.");
			this.saveDefaultConfig();
		}
		activated = Boolean.valueOf(getConfig().getBoolean("activated"));
		if(!activated)
			log.info("BreedFireworks is DEACTIVATED");
		boolean allowMetrics = getConfig().getBoolean("metrics");
		if(allowMetrics){
			log.info("BreedFireworks metrics are ALLOWED");
			try {
				MetricsLite metrics = new MetricsLite(this);
				metrics.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else
			log.info("BreedFireworks metrics are NOT ALLOWED");
	}
	
	@Override
	public void onDisable(){
		getConfig().set("activated", activated);
		saveConfig();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,	String commandLabel, String[] args){
		if(!(commandLabel.equalsIgnoreCase("bf") || commandLabel.equalsIgnoreCase("BreedFireworks"))) return false;
		if(!sender.hasPermission("BreedFireworks.activate")){
			sender.sendMessage(ChatColor.RED + "You don't have permission to use this command.");
			return false;
		}
		if(args.length == 0){
			String out = ChatColor.GOLD + "The plugin is ";
			
			if(activated) out += ChatColor.GREEN + "activated";
			else out += ChatColor.RED + "deactivated";
			
			out += ChatColor.GOLD + ". Use \"/bf ";
			
			if(activated) out += ChatColor.GOLD + "deactivate\" to deactivate.";
			else out += ChatColor.GOLD + "activate\" to activate";
			
			sender.sendMessage(out);
			return true;
		}
		String actionStr = args[0];
		if(actionStr.equalsIgnoreCase("activate")){
			activated = true;
			sender.sendMessage(ChatColor.GOLD + "The plugin has been " + ChatColor.GREEN + "activated");
			return true;
		}
		if(actionStr.equalsIgnoreCase("deactivate")){
			activated = false;
			sender.sendMessage(ChatColor.GOLD + "The plugin has been " + ChatColor.RED + "deactivated");
			return true;
		}
		sender.sendMessage(ChatColor.GOLD + "Unknown argument. Check the spelling.");
		return false;
	}

}
